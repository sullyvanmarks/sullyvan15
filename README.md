### Olá, tudo bem? 👋



Algumas coisas sobre mim:

- Atualmente tenho 19 Anos :)
- Estudante de Ciência da Computação
- Certificados Web Full-stack e Desenvolvimento avançado em Python pela Digital Innovation One - Dezembro de 2019 (Mais informações: https://www.linkedin.com/in/sullyvanmarks/)
- Também faço vídeos sobre tecnologia no geral: https://www.youtube.com/smtechbr

<h2 align="center">Github stats </h2>

[![Github Stats](https://github-readme-stats.vercel.app/api?username=sullyvan15&hide=[%22issues%22,%22prs%22,%22contribs%22]&show_icons=true&theme=default)](https://github.com/sullyvan15)
<h2><img src="https://github-readme-stats.vercel.app/api/top-langs/?username=sullyvan15&langs_count=20&layout=compact" alt="sullyyan15 :: Top linguagens" /></h2>

